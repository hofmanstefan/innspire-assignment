use super::*;
use rust_decimal_macros::dec;

#[test]
fn test_no_duplicates() {
    let record_one: Record = Record {
        reference: 1234,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.00),
    };
    let record_two: Record = Record {
        reference: 1235,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.00),
    };

    let records: Vec<Record> = [record_one, record_two].to_vec();

    assert_eq!(duplicate_references(records).len(), 0);
}

#[test]
fn test_one_duplicates() {
    let record_one: Record = Record {
        reference: 1234,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.00),
    };
    let record_two: Record = Record {
        reference: 1235,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.00),
    };
    let record_three: Record = Record {
        reference: 1234,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.00),
    };

    let records: Vec<Record> = [record_one, record_two, record_three].to_vec();

    assert_eq!(duplicate_references(records).len(), 2);
}

#[test]
fn test_multiple_duplicates() {
    let record_one: Record = Record {
        reference: 1234,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.00),
    };
    let record_two: Record = Record {
        reference: 1235,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.00),
    };
    let record_three: Record = Record {
        reference: 1234,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.00),
    };
    let record_four: Record = Record {
        reference: 1235,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.00),
    };

    let records: Vec<Record> = [record_one, record_two, record_three, record_four].to_vec();

    assert_eq!(duplicate_references(records).len(), 4);
}
