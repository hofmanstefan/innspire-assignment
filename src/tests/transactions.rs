use super::*;
use rust_decimal_macros::dec;

#[test]
fn test_correct_transactions() {
    let record_one: Record = Record {
        reference: 1234,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.00),
    };
    let record_two: Record = Record {
        reference: 1235,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.00),
    };

    let records: Vec<Record> = [record_one, record_two].to_vec();

    assert_eq!(incorrect_transactions(records).len(), 0);
}

#[test]
fn test_one_incorrect_transaction() {
    let record_one: Record = Record {
        reference: 1234,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.00),
    };
    let record_two: Record = Record {
        reference: 1235,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.01),
    };

    let records: Vec<Record> = [record_one, record_two].to_vec();

    assert_eq!(incorrect_transactions(records).len(), 1);
}

#[test]
fn test_two_incorrect_transaction() {
    let record_one: Record = Record {
        reference: 1234,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.00),
    };
    let record_two: Record = Record {
        reference: 1235,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(-1.10),
        end_balance: dec!(9.01),
    };
    let record_three: Record = Record {
        reference: 1235,
        account_number: String::from("number"),
        description: String::from("description"),
        start_balance: dec!(10.10),
        mutation: dec!(1.10),
        end_balance: dec!(9.01),
    };
    let records: Vec<Record> = [record_one, record_two, record_three].to_vec();

    assert_eq!(incorrect_transactions(records).len(), 2);
}
