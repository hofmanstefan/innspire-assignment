use std::{
    error::Error,
    fs::File,
    process,
};
use serde::Deserialize;
use encoding_rs::WINDOWS_1252;
use encoding_rs_io::DecodeReaderBytesBuilder;
use rust_decimal::Decimal;

#[cfg(test)]
pub mod tests;

// The Record struct
#[derive(Debug, Clone, Deserialize)]
struct Record {
    #[serde(rename = "Reference")]
    reference: u32,
    #[serde(rename = "Account Number")]
    account_number: String,
    #[serde(rename = "Description")]
    description: String,
    #[serde(rename = "Start Balance")]
    start_balance: Decimal,
    #[serde(rename = "Mutation")]
    mutation: Decimal,
    #[serde(rename = "End Balance")]
    end_balance: Decimal,
}

// Parse the CSV files
fn parse_csv() -> Result<Vec<Record>, Box<dyn Error>> {
    // Open the record file
    let file = File::open("data/records.csv")?;
    // Add the right encoding
    let transcoded = DecodeReaderBytesBuilder::new()
        .encoding(Some(WINDOWS_1252))
        .build(file);

    // New vector for the records
    let mut records: Vec<Record> = Vec::new();

    // Build the CSV reader and iterate over each record.
    let mut rdr = csv::Reader::from_reader(transcoded);
    for result in rdr.deserialize() {
        // Deserialize the record in the struct Record
        let record: Record = result?;

        records.push(record);
    }
    // Return the records
    Ok(records)
}

// Parse the xml files
fn parse_xml() -> Result<Vec<Record>, Box<dyn Error>> {
    // Read the file as a string
    let text = std::fs::read_to_string("data/records.xml").unwrap();

    // Parse the string as an XML structure
    let doc = match roxmltree::Document::parse(&text) {
        Ok(v) => v,
        Err(e) => {
            println!("Error: {}.", e);
            std::process::exit(1);
        }
    };

    // New vector for the records
    let mut records: Vec<Record> = Vec::new();

    // Iterate over each record
    for node in doc.root().descendants().filter(|n| n.has_tag_name("record")) {
        let reference = node.descendants().find(|n| n.has_tag_name("record")).unwrap()
            .attributes()[0].value()
            .parse::<u32>().unwrap();
        let account_number = node.descendants().find(|n| n.has_tag_name("accountNumber")).unwrap()
            .text().unwrap()
            .parse::<String>().unwrap();
        let description = node.descendants().find(|n| n.has_tag_name("description")).unwrap()
            .text().unwrap()
            .parse::<String>().unwrap();
        let start_balance = node.descendants().find(|n| n.has_tag_name("startBalance")).unwrap()
            .text().unwrap()
            .parse::<Decimal>().unwrap();
        let mutation = node.descendants().find(|n| n.has_tag_name("mutation")).unwrap()
            .text().unwrap()
            .parse::<Decimal>().unwrap();
        let end_balance = node.descendants().find(|n| n.has_tag_name("endBalance")).unwrap()
            .text().unwrap()
            .parse::<Decimal>().unwrap();

        // Store the values in the Record structure
        let record: Record = Record {
            reference,
            account_number,
            description,
            start_balance,
            mutation,
            end_balance,
        };

        records.push(record);
    }
    // Return the records
    Ok(records)
}

// Find and return the duplicate records
fn duplicate_references(records: Vec<Record>) -> Vec<Record> {
    let mut duplicates: Vec<Record> = Vec::new();

    // Iterate over the records
    for r in records.iter() {
        // Filter all the records with the same reference
        let filter_results = records.iter().filter(|rr| r.reference == rr.reference);
        // If there are more that one reference, it is a duplicate
        if filter_results.count() > 1 {
            duplicates.push(r.clone());
        }
    }
    // Return the duplicates
    duplicates
}

// Find the records with incorrect transactions (start_balance _ mutation != end_balance)
fn incorrect_transactions(records: Vec<Record>) -> Vec<Record> {
    let mut incorrect: Vec<Record> = Vec::new();

    // Iterate over the records
    for r in records.iter() {
        // Calculate the correct sum
        let sum: Decimal = r.start_balance + r.mutation;
        // Verify the correct sum is equal to end_balance
        if sum != r.end_balance {
            incorrect.push(r.clone());
        }
    }
    // Return the incorrect records
    incorrect
}

fn main() {
    let mut records: Vec<Record>;

    // Read the csv files
    let csv_records: Result<Vec<Record>, Box<dyn Error>> = parse_csv();
    if let Err(err) = csv_records {
        println!("error running parse_csv: {}", err);
        process::exit(1);
    }

    records = csv_records.unwrap();

    // Read the xml files
    let xml_records: Result<Vec<Record>, Box<dyn Error>> = parse_xml();
    if let Err(err) = xml_records {
        println!("error running parse_xml: {}", err);
        process::exit(1);
    }

    records.append(&mut xml_records.unwrap());

    // Find the duplicates
    let duplicates: Vec<Record> = duplicate_references(records.clone());
    // Find the incorrect records
    let incorrect: Vec<Record> = incorrect_transactions(records);

    // Print the duplicates
    println!("+----------------------+");
    println!("| Duplicate References |");
    println!("+----------------------+");
    for d in duplicates.iter() {
        println!("{}", d.description);
    }

    // Print the incorrect records
    println!("+----------------------+");
    println!("|Incorrect Transactions|");
    println!("+----------------------+");
    for i in incorrect.iter() {
        println!("{}", i.description);
    }
}
