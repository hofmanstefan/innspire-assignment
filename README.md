[![Build Status](https://drone.hofman-consulting.nl/api/badges/hofmanstefan/innspire-assignment/status.svg)](https://drone.hofman-consulting.nl/hofmanstefan/innspire-assignment)

# InnSpire Assignment

## Requirements
The rust toolchain must be installed on your system. This is most easily done by using [rustup.rs](https://rustup.rs).

## Running the project
To run the project use `$ cargo run` inside the top directory.
